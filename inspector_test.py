from requests import put, get, post
from random import randrange, shuffle
from functools import reduce

URL = 'http://localhost:5000/'

test_subjects = { # 10 random uuids with scores from 1 to 5, up to 20 scores each
    randrange(10000): [randrange(1,6) for _ in range(randrange(1,20))] for _ in range(10)
}

expected_ans = {
    k: round(sum(v)/len(v), 2) for k, v in test_subjects.items()
}

input_nested = [[(uuid, score) for score in test_subjects[uuid]] for uuid in test_subjects] # [[(1,1), (1,2), (1,3)], [(2,2), (2,5)], [(3,4), (3,3)]...]
input_flatten = reduce((lambda a,b: a+b), input_nested)  # [(1,1), (1,2), (1,3), (2,2), (2,5), (3,4), (3,3)...]
shuffle(input_flatten) # [(2,2), (1,3), (3,4), ...]

for uuid, score in input_flatten:
    post(URL+str(uuid), data={'score': str(score)})

for uuid in expected_ans:
    app_ans = get(URL+str(uuid)).json()
    print(uuid, app_ans[str(uuid)], expected_ans[uuid])
    if app_ans[str(uuid)] != expected_ans[uuid]:
        raise ValueError
