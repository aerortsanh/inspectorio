from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

inspectors = {
    '0': {
        'total': 3,
        'n': 1
    }
}

class Inspector(Resource):
    def get(self, uuid):
        return {uuid: round(inspectors[uuid]['total']/inspectors[uuid]['n'], 2)}

    def put(self, uuid):
        inspectors[uuid] = {
            'total': int(request.form['score']),
            'n': 1
        }
        return {uuid: round(inspectors[uuid]['total']/inspectors[uuid]['n'], 2)}

    def post(self, uuid):
        if uuid in inspectors:
            inspectors[uuid].update(
                {
                    'total': inspectors[uuid]['total'] + int(request.form['score']),
                    'n': inspectors[uuid]['n'] + 1
                }
            )
        else:
            inspectors[uuid] = {
                'total': int(request.form['score']),
                'n': 1
            }
        return {uuid: round(inspectors[uuid]['total']/inspectors[uuid]['n'], 2)}


api.add_resource(Inspector, '/<string:uuid>')

if __name__ == '__main__':
    app.run(debug=True)