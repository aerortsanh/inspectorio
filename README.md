# Inspectorio

A simple python restful flask application that record scores of each inspector and output the average scores

According to prompt, the app must take in uuid of inspector and their score via POST method. It'd return the average score.

### Database strategy:
As the business requirement does not explicitly state that we need to use each individual score of each inspector, but the whole average score. Therefore we don't need to save every score because it'd take time to calculate the total sum then the mean. The best strategy is to store the total and the number of scores (called n), so everytime there's a post request we can keep on incrementing the total and n

Database inspectors:
uuid: (primary key) string. The id of inspector.
total: integer. The total recorded score of each inspector uuid.
n: integer. The number of POST records made.

### Request Methods
#### GET
Output the mean of the requested uuid by calculating total/n
#### POST
Add the uuid and total as score and n = 1 if the uuid does not exist in database
Increment total by score and n by 1 if the uuid exists in database
#### PUT
Modify the score of the requested uuid, add if not exist.

### Test
The test fabricates random uuid and scores (between 1 and 5) for 10 inspectors, up to 20 scores each. Then perform POST requests on all of them.
Then it calculates the expected average score of each inspector.
Finally it compares the expected average with the requested average from the app, if there's a difference then it'd raise ValueError

### Improvement
There's still a lot of rooms for improvement, but not enough time. For example:
- User authentication. I suppose the inspector scores should remain private and should not be available for anyone to see
- Input validation: return Error code when user input an inappropriate values outside of range 1-5.
...
